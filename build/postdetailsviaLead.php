
<?php

    function convertxmltoarray($response)
    {

        // Loads the XML and parse into array
        $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
        $xml = new SimpleXMLElement($response);

		//$body = $xml->xpath('/envBody')[0];

		$body = $xml->xpath("envBody")[0];

        $array = json_decode(json_encode((array)$body), TRUE);

        return $array;

	//echo "<script>console.log( 'Debug Objects: " . $response . "' );</script>";
	//echo $xml->asXML();
	//print_r($array);

    }



	function updatecustomerdata()
    {
        $location_URL = "https://my327482.crm.ondemand.com/sap/bc/srt/scs/sap/managemarketingleadin?sap-vhost=my327482.crm.ondemand.com";
        $action_URL   = "http://sap.com/xi/A1S/Global/ManageMarketingLeadIn/MaintainBundleRequest";
        $options      = [
                            'soap_version' => SOAP_1_2,
                            'login'        => '_INT_API',
                            'password'     => 'G@teway6',
                            'trace'        => 1,
                            'exceptions'   => 1
                        ];
        $wsdlpath     = "./ManageMarketingLeadIn.wsdl";

        $client = new SoapClient($wsdlpath, $options);

        // these are the fields when update member
        $firstName = $_POST["fname"];
        $lastName = $_POST["lname"];
        $companyName = $_POST["companyName"];
        $phoneNumber = $_POST["phoneNumber"];
        $email = $_POST["email"];
        $jobTitle = $_POST["jobTitle"];

        //<Name>???</Name> error: 3000(/CL_CDA_INCMP/)INC.BOI3NAME-CONTENT missing021
        //<EmployeeResponsibleUUID>???</EmployeeResponsibleUUID> error: (//PAPR/PARTY_ADMIN/)INC.BOI3Please enter a party with role Employee Responsible.021

        try {
          $xml_string = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">
                           <soap:Header/>
                           <soap:Body>
                              <glob:MarketingLeadBundleMaintainRequest_sync>
                                 <MarketingLead actionCode="01">
                                    <Name>LPERP-'. $companyName .'-'. $firstName .' '. $lastName .'</Name>
                                    <OriginTypeCode>Z06</OriginTypeCode>
                                    <StatusCode>01</StatusCode>
                                    <EmployeeInternalID>8000000018</EmployeeInternalID>
                                    <SalesEmployeeResponsibleInternalID>8000000018</SalesEmployeeResponsibleInternalID>
                                    <CompanyName>'. $companyName .'</CompanyName>
                                    <PersonFirstName>'. $firstName .'</PersonFirstName>
                                    <PersonLastName>'. $lastName .'</PersonLastName>
                                      <Address>
                                       <TelephoneFormattedNumberDescription>'. $phoneNumber .'</TelephoneFormattedNumberDescription>
                                       <EmailURI>'. $email .'</EmailURI>
                                       <ContactFunctionalTitleName>'. $jobTitle .'</ContactFunctionalTitleName>
                                    </Address>
                                 </MarketingLead>
                              </glob:MarketingLeadBundleMaintainRequest_sync>
                           </soap:Body>
                        </soap:Envelope>';

            // Send request
            $response = $client->__doRequest($xml_string, $location_URL, $action_URL, SOAP_1_2);

            $array = convertxmltoarray($response);
			//echo "<script>console.log( 'Debug Objects: " . $response . "' );</script>";
      header('Location: thank-you.html');
			print_r($response);
      }
        catch (SoapFault $exception) {
        }
    }

?>

<?php

	$array_result = updatecustomerdata();
?>
