<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>Customer form</title>
</head>

<?php 

    function convertxmltoarray($response)
    {
		 
        // Loads the XML and parse into array
        $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
        $xml = new SimpleXMLElement($response);
		
		//$body = $xml->xpath('/envBody')[0];

		$body = $xml->xpath("envBody")[0];
		   
        $array = json_decode(json_encode((array)$body), TRUE);

        return $array;
		
	//echo "<script>console.log( 'Debug Objects: " . $response . "' );</script>"; 
	//echo $xml->asXML();	
	//print_r($array);
		
    }
 
 function readlead($querylead)
    {
		$location_URL = "https://myXXXXXX.crm.ondemand.com/sap/bc/srt/scs/sap/querymarketingleadin?sap-vhost=myXXXXXX.crm.ondemand.com";
        $action_URL   = "http://sap.com/xi/A1S/Global/QueryMarketingLeadIn/FindByElementsRequest";
        $options      = [
                            'soap_version' => SOAP_1_2,
                            'login'        => 'UserLogin',
                            'password'     => 'UserPass',
                            'trace'        => 1,
                            'exceptions'   => 1
                        ];
        $wsdlpath     = "sapwsdl/CGC_API_QueryLeads_PRD.wsdl";

        $client = new SoapClient($wsdlpath, $options);

		try {
            // customer selection by Internal ID
            $xml_string = '
					<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">
					   <soap:Header/>
					   <soap:Body>
						  <glob:MarketingLeadByElementsQuery_sync>
							 <MarketingLeadSelectionByElements>
								<SelectionByID>
								   <InclusionExclusionCode>I</InclusionExclusionCode>
								   <IntervalBoundaryTypeCode>1</IntervalBoundaryTypeCode>
								   <LowerBoundaryIdentifier>'. $querylead .'</LowerBoundaryIdentifier>
								</SelectionByID>
							 </MarketingLeadSelectionByElements>
						  </glob:MarketingLeadByElementsQuery_sync>
					   </soap:Body>
					</soap:Envelope>
            ';

            // Send request
            $response = $client->__doRequest($xml_string, $location_URL, $action_URL, SOAP_1_2);
            /*=====  End of DO Request Method  ======*/
			
            $array = convertxmltoarray($response);
			return $array;
			
        } catch (SoapFault $exception){
			echo 'query catch';
        }
		
echo 'end query';
       
    }
	
 function querycustomerdata($querydata)
    {
		
		//var_dump(class_exists("SOAPClient"));

		$location_URL = "https://my327023.crm.ondemand.com/sap/bc/srt/scs/sap/querycustomerin1?sap-vhost=my327023.crm.ondemand.com";
        $action_URL   = "http://sap.com/xi/A1S/Global/QueryCustomerIn/FindByCommunicationDataRequest";
        $options      = [
                            'soap_version' => SOAP_1_2,
                            'login'        => 'UserLogin',
                            'password'     => 'UserPass',
                            'trace'        => 1,
                            'exceptions'   => 1
                        ];
        $wsdlpath     = "sapwsdl/CGC_API_QueryCustomers_PRD.wsdl";

        $client = new SoapClient($wsdlpath, $options);

		try {
            // customer selection by Internal ID
            $xml_string = '
            <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global" xmlns:a2e3="http://sap.com/xi/AP/CustomerExtension/BYD/A2E34">
               <soap:Header/>
               <soap:Body>
                  <glob:CustomerByCommunicationDataQuery_sync>
                     <CustomerSelectionByCommunicationData>
                        <SelectionByInternalID>
                           <InclusionExclusionCode>I</InclusionExclusionCode>
                           <IntervalBoundaryTypeCode>1</IntervalBoundaryTypeCode>
                           <LowerBoundaryInternalID>' . $querydata . '</LowerBoundaryInternalID>
                        </SelectionByInternalID>
                     </CustomerSelectionByCommunicationData>
                  </glob:CustomerByCommunicationDataQuery_sync>
               </soap:Body>
            </soap:Envelope>
            ';

            // Send request
            $response = $client->__doRequest($xml_string, $location_URL, $action_URL, SOAP_1_2);
            /*=====  End of DO Request Method  ======*/
//echo  $response;
            $array = convertxmltoarray($response);
			
		//echo $array['n0CustomerByCommunicationDataResponse_sync']['Customer']['UUID'];
		//echo $array['n0CustomerByCommunicationDataResponse_sync']['Customer']['Person']['GivenName'];
		
			return $array;
        } catch (SoapFault $exception){
			echo 'query catch';
        }
		
echo 'end query';
       
    }
	
	
	function updatecustomerdata($customer)
    {
		
		echo 'start update';
		
        $location_URL = "https://my327023.crm.ondemand.com/sap/bc/srt/scs/sap/managecustomerin1?sap-vhost=my327023.crm.ondemand.com";
        $action_URL   = "hhttp://sap.com/xi/A1S/Global/ManageCustomerIn/MaintainBundle_V1Request";
        $options      = [
                            'soap_version' => SOAP_1_2,
                            'login'        => 'UserLogin',
                            'password'     => 'UserPass',
                            'trace'        => 1,
                            'exceptions'   => 1
                        ];
        $wsdlpath     = "sapwsdl/CGC_API_Customers_PRD.wsdl";

        $client = new SoapClient($wsdlpath, $options);

        // these are the fields when update member
        $firstname = 'diana upd';
        $lastname = 'ngoh upd';
        $gender = '1'; 
        $birthday = '1987-11-02';
        $addruuid = '00163e0f-7c7e-1ed6-b0bd-422d49521f7b';
        $email = 'diana.ngoh@brightree.xyz';
        $country = 'MY';
        $state = 'PIN';
        $city = 'Test';
        $zipcode = '11111';
        $address = 'test22';
        $birthdaymonth = '11';
        $icno = '1234';

        try {
            $xml_string = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global" xmlns:a2e3="http://sap.com/xi/AP/CustomerExtension/BYD/A2E34" xmlns:glob1="http://sap.com/xi/AP/Globalization">   
                <soap:Header/>   
                <soap:Body>  
                    <glob:CustomerBundleMaintainRequest_sync_V1>  
                        <!--1 or more repetitions:-->  
                        <Customer actionCode="02"> 

                            <InternalID>' . $customer . '</InternalID>
                            <!-- CategoryCode 1 denotes Individual Customer -->
                            <CategoryCode>1</CategoryCode>  
                            <!-- CustomerIndicator true denotes Customer account -->    
                            <CustomerIndicator>true</CustomerIndicator>     
                            <!-- LifeCycleStatusCode 2 denotes Active -->   
                            <LifeCycleStatusCode>2</LifeCycleStatusCode>    

                            <Person>    
                                <GivenName>' . $firstname . '</GivenName> 
                                <FamilyName>' . $lastname . '</FamilyName>    
                                <!--Gender: 0-Unknown, 1-Male, 2-Female -->
                                <GenderCode>' . $gender . '</GenderCode>
                                <!--BirthDate format YYYY-MM-DD -->
                                <BirthDate>' . $birthday . '</BirthDate>
                            </Person>

                            <AddressInformation actionCode="02">
                                <UUID>' . $addruuid . '</UUID>
                                <Address actionCode="04">
                                    <Email>   
                                        <URI>' . $email . '</URI> 
                                    </Email>  
                                    <PostalAddress>   
                                        <CountryCode>' . $country . '</CountryCode>
                                        <RegionCode>' . $state . '</RegionCode>
                                        <CityName>' . $city . '</CityName>
                                        <StreetPostalCode>' . $zipcode . '</StreetPostalCode>
                                        <StreetName>' . $address . '</StreetName>
                                        <StreetSuffixName></StreetSuffixName>
                                    </PostalAddress>  
                                    <DefaultMobilePhone>' . $phone . '</DefaultMobilePhone>   
                                    <DefaultConventionalPhone></DefaultConventionalPhone>    
                                </Address>   
                            </AddressInformation>
                            
                            <!-- 01 for Jan, 02 for Feb, 03 for Mar, etc -->
                            <a2e3:BirthdayMonth>' . $birthdaymonth . '</a2e3:BirthdayMonth>
                            <!-- YYYY-MM-DD -->
                            <a2e3:NRIC>' . $icno . '</a2e3:NRIC> 
                        </Customer>    
                    </glob:CustomerBundleMaintainRequest_sync_V1> 
                </soap:Body> 
            </soap:Envelope>';


            // Send request
            $response = $client->__doRequest($xml_string, $location_URL, $action_URL, SOAP_1_2);

            $array = convertxmltoarray($response);
			echo "<script>console.log( 'Debug Objects: " . $response . "' );</script>"; 
			print_r($response);
			
        } catch (SoapFault $exception){
            echo 'update catch';
        }
		
		echo 'end update';
    }
	
?>

<?php

$lead_array = readlead($_GET["lead"]);
$lead_c = $lead_array['n0MarketingLeadByElementsResponse_sync']['MarketingLead']['ProspectParty']['AccountInternalID'];

if ($lead_c == $_GET["c"]){
	$array_result = querycustomerdata($lead_c);

	$id = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['InternalID'];
	$fname = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['Person']['GivenName'];
	$lname = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['Person']['FamilyName'];
	$ic = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['n1NRIC'];
	$birthday = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['Person']['BirthDate'];
	$gender = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['Person']['GenderCode'];
	$phone = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['AddressInformation']['Address']['Telephone']['FormattedNumberDescription'];
	$email = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['AddressInformation']['Address']['Email']['URI'];
	$addruuid = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['AddressInformation']['UUID'];	
	$country = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['AddressInformation']['Address']['PostalAddress']['CountryCode'];	
	$state = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['AddressInformation']['Address']['PostalAddress']['RegionCode'];	
	$city = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['AddressInformation']['Address']['PostalAddress']['CityName'];	
	$postcode = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['AddressInformation']['Address']['PostalAddress']['StreetPostalCode'];	
	$address1 = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['AddressInformation']['Address']['PostalAddress']['StreetName'];	
	$address2 = $array_result['n0CustomerByCommunicationDataResponse_sync']['Customer']['AddressInformation']['Address']['PostalAddress']['StreetSuffixName'];	
	
			 
	//if ($gender == "1") {
	//	$gender = "Male";
	//} elseif ($gender == "2") {
	//	$gender = "Female";
	//} else {
	//	$gender = "";
	//}; 
		
} else {
	header("Location: error.html");
	exit; 
}

?>



<body>

<form method="POST" name="form_create" id="form_create" action="postdetailsviaLead.php" role="form">
    <input type="hidden" name="id" 	id="id" 	value=<?php echo $id; ?> /> 
    <table>
    <tr><td><label for="txtfname">First name:</label>
        <td><input type="text" name="fname" id="fname"  value="<?php echo $fname; ?>" /></td></td></tr>
    <tr><td><label for="txtlname">Last name:</label>
    	<td><input type="text" name="lname" id="lname"  value="<?php echo $lname; ?>" /></td></td></tr>
    <tr><td><label for="txtic">IC No.:</label>
        <td><input type="text" name="ic"  id="ic"       value="<?php echo $ic; ?>" /></td></td></tr>
    <tr><td><label for="txtbirthday">Birthday:</label>
        <td><input type="date" name="birthday"    id="birthday"   value="<?php echo $birthday; ?>" /></td></td></tr>
    <tr><td><label for="txtgender">Gender:</label>
        <td><select name="gender" id="gender" >
            <option value = "1" 
                <?php
                if ($gender == 1){
                    echo 'selected="selected"';
                    } 
                ?> >Male</option>
            <option value = "2" 
                <?php
                if ($gender == 2){
                    echo 'selected="selected"';
                    } 
                ?> >Female</option>     
            <option value = "0" 
                <?php
                if ($gender == 0){
                    echo 'selected="selected"';
                    } 
                ?> ></option>   
            </select></td></td></tr>
    <tr><td><label for="txtphone">Telephone:</label>
        <td><input type="text" name="phone"     id="phone"  value="<?php echo $phone; ?>" /> </td></td></tr>
    <tr><td><label for="txtemail">Email:</label>
        <td><input type="text" name="email"     id="email"  value="<?php echo $email; ?>" /></td></td></tr>
    <tr><td><label for="txtaddress">Address:</label>
        <td><input type="text" name="address1"  id="address1"   value="<?php echo $address1; ?>" />
        <td><input type="text" name="address2"  id="address2"   value="<?php echo $address2; ?>"/></td></td></tr>
    <tr><td><label for="txtcity">City: </label>
        <td><input type="text" name="city"  id="city"   value="<?php echo $city; ?>" /></td></td></tr>
    <tr><td><label for="txtapostcode">Postcode:</label>
        <td><input type="text" name="postcode"  id="postcode"   value="<?php echo $postcode; ?>" /></td></td></tr>
    <tr><td><label for="txtstate">State:</label>
        <td><select name="state" id="state">
            <option value = "JOH" <?php if ($state == JOH) { echo 'selected="selected"'; } ?> >JOH - Johor </option>
            <option value = "KED" <?php if ($state == KED) { echo 'selected="selected"'; } ?> >KED - Kedah </option>
            <option value = "KEL" <?php if ($state == KEL) { echo 'selected="selected"'; } ?> >KEL - Kelantan </option>
            <option value = "KUL" <?php if ($state == KUL) { echo 'selected="selected"'; } ?> >KUL - Kuala Lumpur </option>
            <option value = "LAB" <?php if ($state == LAB) { echo 'selected="selected"'; } ?> >LAB - Labuan </option>
            <option value = "MEL" <?php if ($state == MEL) { echo 'selected="selected"'; } ?> >MEL - Melaka </option>            
            <option value = "PAH" <?php if ($state == PAH) { echo 'selected="selected"'; } ?> >PAH - Pahang </option>
            <option value = "PEL" <?php if ($state == PEL) { echo 'selected="selected"'; } ?> >PEL - Perlis </option>
            <option value = "PER" <?php if ($state == PER) { echo 'selected="selected"'; } ?> >PER - Perak </option>
            <option value = "PIN" <?php if ($state == PIN) { echo 'selected="selected"'; } ?> >PIN - Pulau Penang </option>
            <option value = "PJY" <?php if ($state == PJY) { echo 'selected="selected"'; } ?> >PJY - Putrajaya </option>
            <option value = "SAB" <?php if ($state == SAB) { echo 'selected="selected"'; } ?> >SAB - Sabah </option>
            <option value = "SAR" <?php if ($state == SAR) { echo 'selected="selected"'; } ?> >SAR - Sarawak</option>
            <option value = "SEL" <?php if ($state == SEL) { echo 'selected="selected"'; } ?> >SEL - Selangor </option>
            <option value = "SER" <?php if ($state == SER) { echo 'selected="selected"'; } ?> >SER - Negeri Sembilan </option>
            <option value = "TRE" <?php if ($state == TRE) { echo 'selected="selected"'; } ?> >TRE - Terengganu </option></td></td></tr>
    <tr><td><label for="txtcountry">Country:</label>
        <td><input type="text" name="country"   id="country"    value="MY" readonly /></td></td></tr>
    </table>

    <input type="hidden" name="addruuid" 	id="addruuid" 	value="<?php echo $addruuid; ?>" /> 
	<br><br>
	<input type="submit" class="button" name="submit" method="post" id="submit" value="Submit" />
</form>

<?php
//if(isset($_POST['submit'])){
//	$selected_val = $_POST['gender'];  // Storing Selected Value In Variable
//	echo "You have selected :" .$selected_val;  // Displaying Selected Value
//	$gender = $selected_val;
//}
?>
</body>

</html>
