const gulp           = require('gulp');
const concat         = require('gulp-concat');
const sass           = require('gulp-sass');
const uglify         = require('gulp-uglify');
const browserSync    = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');

// Root Dir
const rootDir = '.';

//Resources path
const resourcesDir = rootDir + '/app';

//Resource paths
const resourceStyles = resourcesDir + '/scss';
const resourceFonts = resourcesDir + '/fonts';
const resourceImgs = resourcesDir + '/images';
const resourceScripts = resourcesDir + '/js';
const resourceVideos = resourcesDir + '/videos';
const resourceHtml = resourcesDir + 'html';

//Resources path
const buildDir = rootDir + '/build';

//Build paths
const buildStyles = buildDir + '/css';
const buildFonts = buildDir + '/fonts';
const buildImgs = buildDir + '/images';
const buildScripts = buildDir + '/js';
const buildVideos = buildDir + '/videos';
const htmlFiles = buildDir + '/*.html';

/**
 * compile SASS to CSS
 * @return {[type]}   [description]
 */
gulp.task('sass', function() {
    return gulp.src(resourceStyles + '/all.scss')
        .pipe( sass({
            outputStyle: 'compressed'
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe( gulp.dest( buildStyles ) )
        .pipe( browserSync.stream() );
});

/**
 * compile script
 * @return {[type]} [description]
 */
gulp.task('js', function() {
    return gulp.src(resourceScripts + '/all.js')
        .pipe(uglify())
        .pipe(gulp.dest(buildScripts));
});

/**
 * Copy images
 * @return {[type]} [description]
 */
gulp.task('img', function() {

    return gulp.src(resourceImgs + '/**/*')
        .pipe(gulp.dest(buildImgs));

});

/**
 * Copy fonts
 * @return {[type]} [description]
 */
gulp.task('font', function() {

    return gulp.src(resourceFonts + '/**/*')
        .pipe(gulp.dest(buildFonts));

});

/**
 * Copy videos
 * @return {[type]} [description]
 */
gulp.task('video', function() {

    return gulp.src(resourceVideos + '/**/*')
        .pipe(gulp.dest(buildVideos));

});

gulp.task('html', function() {

    return gulp.src(resourceHtml + '/**/*')
        .pipe(gulp.dest(buildDir))
        .pipe( browserSync.stream() );

});

/**
 * The default task (called when you run `gulp`)
 */
gulp.task('default', [ 'js', 'sass', 'img', 'font', 'video' ]);

/**
 * Watch files and run tasks if they change
 * @return {[type]} [description]
 */
gulp.task('watch', ['default'], function () {
    browserSync.init({
      server: {
          baseDir: buildDir
      }
    });
    gulp.watch(resourceStyles + '/**/*', ['sass']);
    gulp.watch(resourceScripts + '/**/*', ['js']);
    gulp.watch(resourceHtml + '/**/*', ['html']);
    gulp.watch(htmlFiles).on('change', browserSync.reload);
});
